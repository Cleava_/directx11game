#include <d3d11.h>
#include <d3dx11.h>
#include <DxErr.h>
#include <xnamath.h>


class StaticCamera
{

public:

	StaticCamera();
	~StaticCamera();

	XMMATRIX WVP;
	XMMATRIX World;
	XMMATRIX camView;
	XMMATRIX camProjection;

	XMVECTOR camPosition;
	XMVECTOR camTarget;
	XMVECTOR camUp;

	


};

