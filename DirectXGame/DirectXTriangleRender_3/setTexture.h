#include <d3d11.h>
#include <D3DX11.h>
#include <DxErr.h>
#include <xnamath.h>
#include <vector>
using namespace std;

class setTexture
{
public:
	setTexture();
	~setTexture();

	void setTex(char* texture);
	void setDevice(ID3D11Device* d3dDevice);
	void setColorMap(ID3D11ShaderResourceView* colorMap_);
	void setDesc();

	ID3D11Device* d3dDevice_;

	HRESULT d3dResult;

	D3D11_SAMPLER_DESC colorMapDesc;

	ID3D11SamplerState* colorMapSampler_;

	ID3D11ShaderResourceView* colorMap_;

};

