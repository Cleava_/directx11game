#include <D3D11.h>
#include <xnamath.h>

class ArcCamera
{
public:
	ArcCamera();

	void SetDistance(float distance, float minDistance, float maxDistance);
	void SetRotation(float x, float, float minY, float maxY);
	void SetTarget(XMFLOAT3& target);

	void ApplyZoom(float zoomDelta);
	void ApplyRotation(float yawDelta, float pitchDelta);

	XMMATRIX GetViewMatrix();

private:
	XMFLOAT3 position_;
	XMFLOAT3 target_;

	float distance_, minDistance_, maxDistance_;
	float xRotation_, yRotation_, yMin_, yMax_;

};
