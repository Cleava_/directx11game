#ifndef _DEMO_BASE_H_
#define _DEMO_BASE_H_
#define KEYDOWN( name, key ) ( name[key] & 0x80 )

#include<d3d11.h>
#include<d3dx11.h>
#include<DxErr.h>
#include<dinput.h>




class Dx11DemoBase
{
public:
	Dx11DemoBase();
	virtual ~Dx11DemoBase();

	bool Initialize(HINSTANCE hInstance, HWND hwnd);
	void Shutdown();

	bool CompileD3DShader(char* filePath, char* entry, char* shaderModel, ID3DBlob** buffer);

	virtual bool LoadContent();
	virtual void UnloadContent();

	virtual void Update(float dt) = 0;
	virtual void Render() = 0;

protected:
	HINSTANCE hInstance_;
	HWND hwnd_;

	D3D_DRIVER_TYPE driverType_;
	D3D_FEATURE_LEVEL featureLevel_;

	ID3D11Device* d3dDevice_;
	ID3D11DeviceContext* d3dContext_;
	IDXGISwapChain* swapChain_;
	ID3D11RenderTargetView* backBufferTarget_;

	ID3D11Texture2D* depthTexture_;
	ID3D11DepthStencilView* depthStencilView_;

	LPDIRECTINPUT8 directInput_;
	LPDIRECTINPUTDEVICE8 keyboardDevice_;
	char keyboardKeys_[256];
	char prevKeyboardKeys_[256];

};

#endif
