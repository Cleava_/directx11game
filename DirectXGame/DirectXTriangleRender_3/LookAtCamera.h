#include <d3d11.h>
#include <D3DX11.h>
#include <DxErr.h>
#include <xnamath.h>

class LookAtCamera
{

public:
	LookAtCamera();
	LookAtCamera(XMFLOAT3 pos, XMFLOAT3 target);

	void SetPositions(XMFLOAT3 pos, XMFLOAT3 target);
	XMMATRIX GetViewMatrix();

private:
	XMFLOAT3 position_;
	XMFLOAT3 target_;
	XMFLOAT3 up_;
};