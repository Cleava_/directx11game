#define KEYDOWN( name, key ) ( name[key] & 0x80 )

#include "Dx11DemoBase.h"
//#include "LookAtCamera.h"
#include "ArcCamera.h"
#include "PosSet.h"
#include "ObjLoading.h"
#include <XInput.h>
#include <dinput.h>
#include <vector>
using namespace std;


class CubeDemo : public Dx11DemoBase
{

public:
	CubeDemo();
	virtual	~CubeDemo();

	bool LoadContent();
	void UnloadContent();

	void Update(float dt);
	void Render();

	void createMenu();

	void createScene();

	int location = 0;
	bool switchScene = false;

private: 

	ID3D11VertexShader* solidColorVS_;
	ID3D11PixelShader* solidColorPS_;

	ID3D11InputLayout* inputLayout_;
	ID3D11Buffer* vertexBuffer_;

	ID3D11Buffer* viewCB_;
	ID3D11Buffer* projCB_;
	ID3D11Buffer* worldCB_;
	XMMATRIX projMatrix_;

public: // ALL VECTORS

	ObjLoading loadPicker;


private: // OBJLOADING
	
 vector<ObjLoading> objLoading;


private: // ARCAMERA PRIVATE

	ArcCamera camera_;

private: // KEYBOARD

	LPDIRECTINPUT8 directInput_;
	char keyboardKeys_[256];
	char prevKeyboardKeys_[256];


private: // CONTROLLER
	XINPUT_STATE controller1State_;
	XINPUT_STATE prevController1State_;
};

