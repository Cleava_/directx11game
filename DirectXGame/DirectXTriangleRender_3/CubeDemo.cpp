#include"CubeDemo.h"
#include "ObjLoading.h"
#include <d3dcompiler.h>
#include<xnamath.h>
#include<cmath>


CubeDemo::CubeDemo() : solidColorVS_(0), solidColorPS_(0), inputLayout_(0), viewCB_(0), projCB_(0), worldCB_(0)
{
	ZeroMemory(&controller1State_, sizeof(XINPUT_STATE));
	ZeroMemory(&prevController1State_, sizeof(XINPUT_STATE));
}


CubeDemo::~CubeDemo()
{

}

bool CubeDemo::LoadContent()
{
	ID3DBlob* vsBuffer = 0;

	bool compileResult = CompileD3DShader("ColorShader.fx", "VS_Main", "vs_4_0", &vsBuffer);

	if (compileResult == false)
	{
		DXTRACE_MSG("Error compiling the vertex shader!");
		return false;
	}

	HRESULT d3dResult;

	d3dResult = d3dDevice_->CreateVertexShader(vsBuffer->GetBufferPointer(),
		vsBuffer->GetBufferSize(), 0, &solidColorVS_);

	if (FAILED(d3dResult))
	{
		DXTRACE_MSG("Error creating the vertex shader!");

		if (vsBuffer)
			vsBuffer->Release();

		return false;
	}

	D3D11_INPUT_ELEMENT_DESC solidColorLayout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	
	};

	unsigned int totalLayoutElements = ARRAYSIZE(solidColorLayout);

	d3dResult = d3dDevice_->CreateInputLayout(solidColorLayout, totalLayoutElements,
		vsBuffer->GetBufferPointer(), vsBuffer->GetBufferSize(), &inputLayout_);

	vsBuffer->Release();

	if (FAILED(d3dResult))
	{
		DXTRACE_MSG("Error creating the input layout!");
		return false;
	}

	ID3DBlob* psBuffer = 0;

	compileResult = CompileD3DShader("ColorShader.fx", "PS_Main", "ps_4_0", &psBuffer);

	if (compileResult == false)
	{
		DXTRACE_MSG("Error compiling pixel shader!");
		return false;
	}

	d3dResult = d3dDevice_->CreatePixelShader(psBuffer->GetBufferPointer(),
		psBuffer->GetBufferSize(), 0, &solidColorPS_);

	psBuffer->Release();

	if (FAILED(d3dResult))
	{
		DXTRACE_MSG("Error creating pixel shader!");
		return false;
	}

	// Loading Menu @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

	createMenu();

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

	D3D11_BUFFER_DESC constDesc;
	ZeroMemory(&constDesc, sizeof(constDesc));
	constDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	constDesc.ByteWidth = sizeof(XMMATRIX);
	constDesc.Usage = D3D11_USAGE_DEFAULT;

	d3dResult = d3dDevice_->CreateBuffer(&constDesc, 0, &viewCB_);

	if (FAILED(d3dResult))
	{
		return false;
	}

	d3dResult = d3dDevice_->CreateBuffer(&constDesc, 0, &projCB_);

	if (FAILED(d3dResult))
	{
		return false;
	}

	d3dResult = d3dDevice_->CreateBuffer(&constDesc, 0, &worldCB_);

	if (FAILED(d3dResult))
	{
		return false;
	}


	// MENU CAMERA
	
	projMatrix_ = XMMatrixPerspectiveFovLH(XM_PIDIV4, 800.0f / 600.0f, 0.01f, 100.0f);  //XMMatrixOrthographicLH(40.0f, 30.0f, 0.01f, 100.0f);
	projMatrix_ = XMMatrixTranspose(projMatrix_);

	camera_.SetDistance(1.0f, 14.0f, 500.0f); //camera_.SetPositions(XMFLOAT3(0.5f, 30.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, 0.0f));


	return true;
}


void CubeDemo::UnloadContent()
{
	//if (colorMapSampler_) colorMapSampler_->Release();
	if (solidColorVS_) solidColorVS_->Release();
	if (solidColorPS_) solidColorPS_->Release();
	if (inputLayout_) inputLayout_->Release();
	if (viewCB_) viewCB_->Release();
	if (projCB_) projCB_->Release();
	if (worldCB_) worldCB_->Release();
	if (vertexBuffer_) vertexBuffer_->Release();

	//colorMapSampler_ = 0;
	solidColorVS_ = 0;
	solidColorPS_ = 0;
	inputLayout_ = 0;
	viewCB_ = 0;
	projCB_ = 0;
	worldCB_ = 0;
	vertexBuffer_ = 0;


}

void CubeDemo::Update(float dt)
{
	keyboardDevice_->GetDeviceState(sizeof(keyboardKeys_), (LPVOID)&keyboardKeys_);

	SHORT tabKeyState = GetAsyncKeyState(DIK_LEFT);

	XMFLOAT3 original(3.0f, 2.0f, -2.0f);
	XMFLOAT3 next(-3.0f, 2.0f, -2.0f);

	float yawDelta = 0.0f;
	float pitchDelta = 0.0f;

	// CAMERA MOVEMENT //
	if (switchScene == false)
	{
		if (objLoading.size() != 0)
		{
			if (KEYDOWN(prevKeyboardKeys_, DIK_LEFT) && !KEYDOWN(keyboardKeys_, DIK_LEFT))
			{
				objLoading.pop_back();
				location = 1;
				loadPicker.movement.SetPosition(next);

				objLoading.push_back(loadPicker);
			}
			if (KEYDOWN(prevKeyboardKeys_, DIK_RIGHT) && !KEYDOWN(keyboardKeys_, DIK_RIGHT))
			{
				objLoading.pop_back();
				location = 0;
				loadPicker.movement.SetPosition(original);

				objLoading.push_back(loadPicker);
			}
			if (location == 1)
			{
				if (KEYDOWN(prevKeyboardKeys_, DIK_SPACE) && !KEYDOWN(keyboardKeys_, DIK_SPACE))
				{
					PostQuitMessage(0);
				}
			}
		}
	}
	if (location == 0)
	{
		if (KEYDOWN(prevKeyboardKeys_, DIK_SPACE) && !KEYDOWN(keyboardKeys_, DIK_SPACE))
		{
			objLoading.clear();
			createScene(); // Create Game Scene
			switchScene = true;
			yawDelta += -0.4f;
			pitchDelta += 2.0f;
			camera_.ApplyRotation(yawDelta, pitchDelta);
			//location = 1;
		}
	}
	if (KEYDOWN(prevKeyboardKeys_, DIK_ESCAPE) && !KEYDOWN(keyboardKeys_, DIK_ESCAPE))
	{
		PostQuitMessage(0);
	}

	memcpy(prevKeyboardKeys_, keyboardKeys_, sizeof(keyboardKeys_));

}


void CubeDemo::Render()
{
	if (d3dContext_ == 0)
		return;

	float clearColor[4] = { 0.0f, 0.0f, 0.1f, 1.0f };
	d3dContext_->ClearRenderTargetView(backBufferTarget_, clearColor);
	d3dContext_->ClearDepthStencilView(depthStencilView_, D3D11_CLEAR_DEPTH, 1.0f, 0);

	XMMATRIX viewMat = camera_.GetViewMatrix();
	viewMat = XMMatrixTranspose(viewMat);

	unsigned int stride = sizeof(ObjLoading::VertexPos);
	unsigned int offset = 0;

	for (int i = 0; i < objLoading.size(); i++)
	{
		XMMATRIX world = objLoading[i].movement.GetWorldMatrix();
		world = XMMatrixTranspose(world);

		d3dContext_->IASetInputLayout(inputLayout_);
		d3dContext_->IASetVertexBuffers(0, 1, &objLoading[i].objBuffer_, &stride, &offset);

		d3dContext_->VSSetShader(solidColorVS_, 0, 0);
		d3dContext_->PSSetShader(solidColorPS_, 0, 0);
		d3dContext_->PSSetSamplers(0, 1, &objLoading[i].texture.colorMapSampler_);
		d3dContext_->PSSetShaderResources(0, 1, &objLoading[i].texture.colorMap_);

		d3dContext_->UpdateSubresource(worldCB_, 0, 0, &world, 0, 0);
		d3dContext_->UpdateSubresource(viewCB_, 0, 0, &viewMat, 0, 0);
		d3dContext_->UpdateSubresource(projCB_, 0, 0, &projMatrix_, 0, 0);


		d3dContext_->VSSetConstantBuffers(0, 1, &worldCB_);
		d3dContext_->VSSetConstantBuffers(1, 1, &viewCB_);
		d3dContext_->VSSetConstantBuffers(2, 1, &projCB_);

		d3dContext_->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		d3dContext_->Draw(objLoading[i].totalVerts_, 0);
	}
	swapChain_->Present(0, 0);
}

void CubeDemo::createMenu()
{
	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

	ObjLoading loadPlane;

	XMFLOAT3 setPlane1Move(0.0f, 0.0f, -2.0f);
	XMFLOAT3 setPlane1Scale(2.3f, 2.3f, 2.3f);
	float InitialRot = 180.0f;
	// Converts from degrees to radians.

	loadPlane.setDevice(d3dDevice_);
	loadPlane.setVerts("backgroundPlane.obj");

	loadPlane.movement.SetPosition(setPlane1Move);
	loadPlane.movement.SetScale(setPlane1Scale);
	loadPlane.movement.SetRotation(0.0f, 0.0f, InitialRot);

	loadPlane.texture.setDevice(d3dDevice_);
	loadPlane.texture.setColorMap(loadPlane.texture.colorMap_);
	loadPlane.texture.setTex("mBackground.bmp");

	objLoading.push_back(loadPlane);

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

	ObjLoading loadQuit;

	loadQuit.setDevice(d3dDevice_);
	loadQuit.setVerts("texQuit.obj");
	
	XMFLOAT3 setQuit2Move(-3.0f, 0.0f, -2.0f);
	XMFLOAT3 setQuit2Scale(1.0f, 1.0f, 1.0f);
	
	loadQuit.movement.SetPosition(setQuit2Move);
	loadQuit.movement.SetScale(setQuit2Scale);

	loadQuit.texture.setDevice(d3dDevice_);
	loadQuit.texture.setColorMap(loadQuit.texture.colorMap_);
	loadQuit.texture.setTex("whiteTex.bmp");

	objLoading.push_back(loadQuit);

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

	ObjLoading loadStart;

	XMFLOAT3 setStart3Move(3.0f, 0.0f, -2.0f);
	XMFLOAT3 setStart3Scale(1.0f, 1.0f, 1.0f);

	loadStart.setDevice(d3dDevice_);
	loadStart.setVerts("texStart.obj");

	loadStart.movement.SetPosition(setStart3Move);
	loadStart.movement.SetScale(setStart3Scale);

	loadStart.texture.setDevice(d3dDevice_);
	loadStart.texture.setColorMap(loadStart.texture.colorMap_);
	loadStart.texture.setTex("whiteTex.bmp");

	objLoading.push_back(loadStart);

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

	XMFLOAT3 setPickerMove(3.0f, 2.0f, -2.0f);
	XMFLOAT3 setPickerScale(1.0f, 1.0f, 1.0f);

	loadPicker.setDevice(d3dDevice_);
	loadPicker.setVerts("picker.obj");
	
	loadPicker.movement.SetPosition(setPickerMove);
	loadPicker.movement.SetScale(setPickerScale);

	loadPicker.texture.setDevice(d3dDevice_);
	loadPicker.texture.setColorMap(loadPicker.texture.colorMap_);
	loadPicker.texture.setTex("yellow.bmp");

	objLoading.push_back(loadPicker);

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
}

void CubeDemo::createScene()
{
	ObjLoading loadMap;

	XMFLOAT3 loadMapScale(2.0f, 2.0f, 2.0f);
	XMFLOAT3 loadMapPos(0.0f, 0.0f, 0.0f);
	float sumoStart = 0.01f;

	loadMap.setDevice(d3dDevice_);
	loadMap.setVerts("sumoRing.obj");

	loadMap.movement.SetScale(loadMapScale);
	loadMap.movement.SetPosition(loadMapPos);
	//loadMap.movement.SetRotation(0.0f, sumoEnd, 0.0f);

	loadMap.texture.setDevice(d3dDevice_);
	loadMap.texture.setColorMap(loadMap.texture.colorMap_);
	loadMap.texture.setTex("sumoFloor.png");

	objLoading.push_back(loadMap);

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

	ObjLoading loadSumo1;

	XMFLOAT3 loadSumo1Scale(1.5f, 1.5f, 1.5f);
	XMFLOAT3 loadSumo1Pos(-0.5f, 1.5f, 2.0f);
	float sumoCharStart = -105.0f;

	loadSumo1.setDevice(d3dDevice_);
	loadSumo1.setVerts("sumoDude.obj");

	loadSumo1.movement.SetPosition(loadSumo1Pos);
	loadSumo1.movement.SetScale(loadSumo1Scale);
	loadSumo1.movement.SetRotation(0.0f, sumoCharStart, 0.0f);
	
	loadSumo1.texture.setDevice(d3dDevice_);
	loadSumo1.texture.setColorMap(loadSumo1.texture.colorMap_);
	loadSumo1.texture.setTex("sumoPantsWhite.png");

	objLoading.push_back(loadSumo1);

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

	ObjLoading loadSumo2;

	XMFLOAT3 loadSumo2Scale(1.5f, 1.5f, 1.5f);
	XMFLOAT3 loadSumo2Pos(0.5f, 1.5f, -2.0f);
	float sumoChar2Start = 75.0f;

	loadSumo2.setDevice(d3dDevice_);
	loadSumo2.setVerts("sumoDude.obj");
	
	loadSumo2.movement.SetPosition(loadSumo2Pos);
	loadSumo2.movement.SetScale(loadSumo2Scale);
	loadSumo2.movement.SetRotation(0.0f, sumoChar2Start, 0.0f);

	loadSumo2.texture.setDevice(d3dDevice_);
	loadSumo2.texture.setColorMap(loadSumo2.texture.colorMap_);
	loadSumo2.texture.setTex("sumoDudeBlack.png");

	objLoading.push_back(loadSumo2);

}