#pragma once
#include <D3D11.h>
#include <D3DX11.h>
#include <DxErr.h>
#include <xnamath.h>
class vertexPos
{

	XMFLOAT3 pos;

public:
	
	vertexPos();
	vertexPos(float x, float y, float z);
	~vertexPos();
};

