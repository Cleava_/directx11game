#include "setTexture.h"

int i = 0;


setTexture::setTexture()
{

}


setTexture::~setTexture()
{

}

void setTexture::setDevice(ID3D11Device* d3dDevice)
{
	d3dDevice_ = d3dDevice;
}

void setTexture::setColorMap(ID3D11ShaderResourceView* colorMap)
{
	colorMap_ = colorMap;
}

void setTexture::setTex(char* texture)
{
		
	d3dResult = D3DX11CreateShaderResourceViewFromFile(d3dDevice_, texture, 0, 0, &colorMap_, 0);
	
	setDesc();
}

void setTexture::setDesc()
{

	ZeroMemory(&colorMapDesc, sizeof(colorMapDesc));
	colorMapDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	colorMapDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	colorMapDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	colorMapDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	colorMapDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	colorMapDesc.MinLOD = 0;
	colorMapDesc.MaxLOD = D3D11_FLOAT32_MAX;

	d3dResult = d3dDevice_->CreateSamplerState(&colorMapDesc, &colorMapSampler_);

}


