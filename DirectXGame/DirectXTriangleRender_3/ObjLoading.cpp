#include "ObjLoading.h"



ObjLoading::ObjLoading()
{
	
}

ObjLoading::~ObjLoading()
{

}

void ObjLoading::setDevice(ID3D11Device* device)
{
	d3dDevice = device;
}

void ObjLoading::setVerts(char* load)
{
	loadingObj.LoadOBJ(load);
	
    totalVerts_ = loadingObj.GetTotalVerts();

	vertices = new VertexPos[totalVerts_];
	float* vertsPtr = loadingObj.GetVertices();
	float* texCPtr = loadingObj.GetTexCoords();


	for (int i = 0; i < totalVerts_; i++)
	{
		vertices[i].pos = XMFLOAT3(*(vertsPtr + 0), *(vertsPtr + 1), *(vertsPtr + 2));
		vertsPtr += 3;

		vertices[i].tex0 = XMFLOAT2(*(texCPtr + 1), *(texCPtr + 0));
		texCPtr += 2;
	}

	BuildVBuffer();

	objModel.push_back(loadingObj);
}

void ObjLoading::BuildVBuffer()
{
	HRESULT d3dResult;

	D3D11_BUFFER_DESC vertexDesc;
	ZeroMemory(&vertexDesc, sizeof(vertexDesc));
	vertexDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexDesc.ByteWidth = sizeof(VertexPos) * totalVerts_;

	D3D11_SUBRESOURCE_DATA resourceData;
	ZeroMemory(&resourceData, sizeof(resourceData));
	resourceData.pSysMem = vertices;

	d3dResult = d3dDevice->CreateBuffer(&vertexDesc, &resourceData, &objBuffer_);

	delete[] vertices; 
	loadingObj.Release();

}


