#include <d3d11.h>
#include <d3dx11.h>
#include "PosSet.h"

PosSet::PosSet()
{

	scale_.x = scale_.y = scale_.z = 1.0f;

}

PosSet::~PosSet()
{

}

XMMATRIX PosSet::GetWorldMatrix()
{
	//rotationX_ += 0.0001f;
	//rotationY_ += 0.0001f;
	//rotationZ_ += 0.0001f;
	XMMATRIX translation = XMMatrixTranslation(position_.x, position_.y, position_.z);
	XMMATRIX rotation = XMMatrixRotationRollPitchYaw(rotationX_, rotationY_, rotationZ_);
	XMMATRIX scale = XMMatrixScaling(scale_.x, scale_.y, scale_.z);

	return scale * rotation * translation ;

}

void PosSet::SetPosition(XMFLOAT3& position)
{
	position_ = position;
}

void PosSet::SetRotation(float rotationX, float rotationY, float rotationZ)
{
	rotationX_ = 3.141592 / 180 * rotationX;
	rotationY_ = 3.141592 / 180 * rotationY;
	rotationZ_ = 3.141592 / 180 * rotationZ;
}

void PosSet::SetScale(XMFLOAT3& scale)
{
	scale_ = scale;
}