#pragma once
#include <D3D11.h>
#include <D3DX11.h>
#include <DxErr.h>
#include <xnamath.h>
#include <vector>
#include "ObjModel.h"
#include "PosSet.h"
#include "setTexture.h"
#include <vector>
using namespace std;

class ObjLoading
{
public:
	ObjLoading();
	~ObjLoading();

	void setDevice(ID3D11Device* device);

	struct VertexPos
	{
		XMFLOAT3 pos;
		XMFLOAT2 tex0;
	};

	int totalVerts_;

	vector<ObjModel> objModel;
	ObjModel loadingObj;

	VertexPos* vertices;

	void setVerts(char* load);

private:

	void BuildVBuffer();

public:

	ID3D11Buffer* objBuffer_;

	ID3D11Device* d3dDevice;

public:

	PosSet movement;

	setTexture texture;

};

