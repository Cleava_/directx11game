#define KEYDOWN( name, key ) ( name[key] & 0x80 )

#include "Dx11DemoBase.h"
#include "ArcCamera.h"
#include "CubeDemo.h"
#include <dinput.h>

void KeyBoardC()
{
	


	float yawDelta = 0.0f;
	float pitchDelta = 0.0f;

	HRESULT res = keyboardDevice_->GetDeviceState(sizeof(keyboardKeys_), (LPVOID)&keyboardKeys_);

	SHORT tabKeyState = GetAsyncKeyState(DIK_LEFT);

	if (GetAsyncKeyState(VK_ESCAPE))
	{
		PostQuitMessage(0);
	}

	if (KEYDOWN(prevKeyboardKeys_, DIK_W))
	{
		camera_.ApplyZoom(-0.01f);
	}
	if (KEYDOWN(prevKeyboardKeys_, DIK_S))
	{
		camera_.ApplyZoom(0.01f);
	}
	if (KEYDOWN(prevKeyboardKeys_, DIK_UP))
	{
		yawDelta = 0.0001f;
	}
	if (KEYDOWN(prevKeyboardKeys_, DIK_DOWN))
	{
		yawDelta = -0.0001f;
	}
	if (KEYDOWN(prevKeyboardKeys_, DIK_LEFT))
	{
		pitchDelta = 0.0001f;
	}
	if (KEYDOWN(prevKeyboardKeys_, DIK_RIGHT))
	{
		pitchDelta = -0.0001f;
	}

	camera_.ApplyRotation(yawDelta, pitchDelta);

	memcpy(prevKeyboardKeys_, keyboardKeys_, sizeof(keyboardKeys_));
}