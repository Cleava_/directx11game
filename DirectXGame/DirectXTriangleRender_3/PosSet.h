#ifndef _POS_SET_H_
#define _POS_SET_H_
#include"Dx11DemoBase.h"
#include<xnamath.h>


class PosSet
{
public:
	PosSet();
	virtual ~PosSet();


	XMMATRIX Rotation();
	XMMATRIX GetWorldMatrix();

	void SetPosition(XMFLOAT3& position);
	void SetRotation(float rotationX_, float rotationY_, float rotationZ);
	void SetScale(XMFLOAT3& scale);


private:
	XMFLOAT3 position_;
	float rotationZ_;
	float rotationX_;
	float rotationY_;
	XMFLOAT3 scale_;
};

#endif
